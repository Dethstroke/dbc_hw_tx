/// DBC file: ../_can_dbc/243.dbc    Self node: 'IO'  (ALL = 0)
/// This file can be included by a source file, for example: #include "generated.h"
#ifndef __DBC_EXTERNS
#define __DBC_EXTERNS




extern bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8]);
extern bool initialize_can();




#endif
