

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "string.h"
#include "dbc_externs.h"
#include "can.h"
#include "_can_dbc/generated_can.h"


//extern const uint32_t           MOTOR_STATUS__MIA_MS;
//extern const MOTOR_STATUS_t     MOTOR_STATUS__MIA_MSG;
//extern const uint32_t                             SENSOR_SONARS_m0__MIA_MS;
//extern const SENSOR_SONARS_m0_t                   SENSOR_SONARS_m0__MIA_MSG;
//extern const uint32_t                             SENSOR_SONARS_m1__MIA_MS;
//extern const SENSOR_SONARS_m1_t                   SENSOR_SONARS_m1__MIA_MSG;

//void init_const_values()
//{
//}

bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8])
{
    can_msg_t can_msg = { 0 };
    can_msg.msg_id                = mid;
    can_msg.frame_fields.data_len = dlc;
    memcpy(can_msg.data.bytes, bytes, dlc);

    return CAN_tx(can1, &can_msg, 0);
}

bool initialize_can()
{
    bool can1_init, can2_init;

    if(CAN_init(can2, 100, 100, 100, NULL, NULL)) can1_init = true;
    else can1_init = false;
    if(CAN_init(can1, 100, 100, 100, NULL, NULL)) can2_init = true;
    else can2_init = false;
    CAN_bypass_filter_accept_all_msgs();
    CAN_reset_bus(can2);
    CAN_reset_bus(can1);

    if (can1_init&&can2_init)return true;
    else return false;
}
